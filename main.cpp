#include <vector>
#include <algorithm>

/* Problem:
Given information about research paper citations as std::vector<int>, 
where each element represents the number of citations for a paper, 
calculate the h-index: https://en.wikipedia.org/wiki/H-index.

Your solution should run in O(n).
*/

// Your solution:
int h_index(const std::vector<int>& citations) {
    // Make copy of vector.
    std::vector<int> citations_vector = citations;

    // If only one publication, h_index can be only 1
    // if publication has at least one citation; otherwise 0
    if (citations_vector.size() == 1) {
        return citations_vector.front() > 0 ? 1 : 0;
    }

    // Possible maximum h_index is the amount of publications.
    int max_h_index = citations_vector.size();

    // Get smallest amount of citations.
    auto smallest_citations = std::min_element(citations_vector.begin(), citations_vector.end());

    // If smallest citations is equal or greater than publication's size, then
    // the author has the highest h_index possible.
    if (*smallest_citations >= max_h_index) {
        return max_h_index;
    }

    // Sort vector in descending order.
    std::sort(citations_vector.begin(), citations_vector.end());

    // There is more than one publication, let's analyze them.
    // Starting h_index value is 0.
    int h_index_value = 0;

    // Loop until h_index matches the maximum possible h_index (edge case).
    while (h_index_value != max_h_index) {

        // If copy vector is empty, break out of the loop.
        if (citations_vector.size() == 0) {
            break;
        }

        // Get the current number of citations, and remove it from the copy vector.
        int current_citations = citations_vector.back();
        citations_vector.pop_back();

        // If this publication has no citations, continue to the next one.
        if (current_citations == 0) {
            continue;
        }

        // If this publication has more citations than our current h_index value,
        // this means our h_index must increase.
        if (current_citations > h_index_value) {
            h_index_value++;
        }
    }
    return h_index_value;
}

#include <cassert>

int main() {
    std::vector<int> citations{1};
    assert(h_index(citations) == 1);

    citations.push_back(2);
    citations.push_back(2);
    assert(h_index(citations) == 2);

    citations.push_back(4);
    citations.push_back(4);
    citations.push_back(3);
    assert(h_index(citations) == 3);

    citations = {4,4,4,4,4};
    assert(h_index(citations) == 4);

    citations = {4,3,3,3,3};
    assert(h_index(citations) == 3);

    citations = {100,95,90,85,1};
    assert(h_index(citations) == 4);
}